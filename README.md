# frontend

Este proyecto contiene la capa de presentación para la prueba técnica correspondiente.

## Requisitos

Este proyecto fue creado con las siguientes herramientas sobre Windows 11:
- NodeJs 8.5.5
- Angular CLI 13.3.6


## Requisitos previos

Tener una base de datos con la estructura creada en el proyecto https://gitlab.com/colombia-compra-eficiente/database
Contar con el proyecto backend disponible en el repositorio https://gitlab.com/colombia-compra-eficiente/backend y tenerlo disponible en la url http://localhost:8080 


## Getting started

Desde una consola CMD se navega hasta la raíz del proyecto donde se haya clonado. Una vez ubicado ahí, se debe ejecutar el comando:
```
npm install
```

Con la ejecución de este comando, se inicia la compilación del proyecto.

Posteriormente se realiza el despliegue del proyecto en el servidor embebido con el que cuenta, ejecutando el siguiente comando:
```
ng serve
```

Así, debe quedar disponible en la url http://localhost:4200.

Si se realiza la prueba con la base de datos recomendada y el backend recomendado, se puede ingresar con los datos: email: pepeperez@gmail.com password: 123456
