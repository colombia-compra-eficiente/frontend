import { Component, OnInit } from '@angular/core';
import {MessageService} from "../services/message.service";
import {ClientService} from "../services/client.service";
import {ClientDto} from "../dto/clientDto";

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {

  clients: ClientDto[] = [];
  selectedClient?: ClientDto;

  constructor(private clientService: ClientService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getClients();
  }

  onSelect(client: ClientDto): void {
    this.selectedClient = client;
    this.messageService.add(`ClientsComponent: Selected client id=${client.id}`);
  }

  getClients(): void {
    this.clientService.getAllClient()
        .subscribe((clients) => this.clients = clients as ClientDto[]);
  }

}
