export interface ClientDto {
    id: number;
    name: string;
    email: string;
    password: string;
}