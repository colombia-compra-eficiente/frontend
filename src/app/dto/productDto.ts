export interface ProductDto {
    id: number;
    name: string;
    stock: number;
    price: number;
}