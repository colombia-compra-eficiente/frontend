import {ClientDto} from "./clientDto";
import {ProductDto} from "./productDto";

export interface ShoppingCartDto {
    id: number;
    amount: number;
    paid: boolean;
    creationDate: Date;
    client: ClientDto;
    product: ProductDto;
}