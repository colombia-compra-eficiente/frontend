import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {ClientService} from "../services/client.service";
import {ClientDto} from "../dto/clientDto";
import {first} from "rxjs";
import {AlertService} from "../services/alert.service";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submitted = false;

  email: string = "";
  password: string = "";
  clientLogged: ClientDto = <ClientDto>{ };

  errorMessage: string = "";


  constructor(
      private clientService: ClientService,
      private route: ActivatedRoute,
      private router: Router,
      private alertService: AlertService
  ) { }

  ngOnInit() { }

  login() {
    this.submitted = true;
    this.clientService.getClientLogin(this.email, this.password).subscribe(
        data => {
            this.clientLogged = data;
            this.router.navigateByUrl('products/' + this.clientLogged.id);
        },
        error => {
            console.log("error", error.error);
            this.errorMessage = error.error;
        });

  }
}

