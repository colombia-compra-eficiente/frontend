import { Component, OnInit } from '@angular/core';
import {ClientDto} from "../dto/clientDto";
import {ActivatedRoute} from "@angular/router";
import {ClientService} from "../services/client.service";
import {ProductDto} from "../dto/productDto";
import {ProductService} from "../services/product.service";

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  client: ClientDto | undefined;

  products: ProductDto[] = [];

  constructor(
      private route: ActivatedRoute,
      private clientService: ClientService,
      private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.getCurrentClient();
    this.getProducts();
  }

  getCurrentClient(): void {
    const routeParams = this.route.snapshot.paramMap;
    const clientIdFromRoute = Number(routeParams.get('clientId'));

    this.clientService.getClient(clientIdFromRoute).subscribe(
        data => {
          this.client = data;
        }
    );
  }

  getProducts(): void {
    //this.productService.getAllProduct()
    //    .subscribe((products) => this.products = products as ProductDto[]);
    this.productService.getAllProduct().subscribe(
        data => {
          this.products = data as ProductDto[];
        }
    );
  }

}
