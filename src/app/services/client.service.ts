import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, map, Observable} from "rxjs";
import {MessageService} from "./message.service";
import {ClientDto} from "../dto/clientDto";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private clientUrl = 'http://localhost:8080/client';

  constructor(
      private http: HttpClient,
      private messageService: MessageService
  ) {  }

  /** Log a ClientService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`ClientService: ${message}`);
  }

  getAllClient(): Observable<ClientDto[]> {
    return this.http.get<ClientDto[]>(this.clientUrl + "/all");
  }

  getClient(id: number): Observable<ClientDto> {
    return this.http.get<ClientDto>(this.clientUrl  + "/" + id);
  }

  getClientLogin(email: string, password: string): Observable<ClientDto> {
    return this.http.get<ClientDto>(this.clientUrl + "/login?email=" + email + "&password=" + password );
  }
}
