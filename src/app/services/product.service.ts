import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MessageService} from "./message.service";
import {Observable} from "rxjs";
import {ProductDto} from "../dto/productDto";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productUrl = 'http://localhost:8080/product';

  constructor(
      private http: HttpClient,
      private messageService: MessageService
  ) { }

  private log(message: string) {
    this.messageService.add(`ProductService: ${message}`);
  }

  getAllProduct(): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(this.productUrl + "/all");
  }
}
